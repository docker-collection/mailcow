# Mailcow

Copy of the official Mailcow Docker stack **with ARM support**

## Changes made to the original

- The package `sogo-activesync` inside SoGo cannot be installed
  using the official Dockerfile and is therefore ignored.
- Using `arm32v7/solr:7.7-slim` instead of `solr:7.7-slim` for
  arm support.
- The package `dovecot-lua` cannot be installed and was replaced
  by the (hopefully equivalent) package `dovecot-auth-lua`.

*Note: `mailcow-github` is an exact copy of the official repo.*
